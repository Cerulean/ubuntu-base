FROM nvidia/cuda:8.0-cudnn6-runtime

ENV BASE_DIR  /base
ENV TALIB_DIR /base/dependencies/ta-lib

WORKDIR $BASE_DIR

ADD . $BASE_DIR

RUN /bin/bash -c 'apt-get update'
RUN /bin/bash -c 'apt-get install -y python3.5 python3-pip python3-tk openssl libssl-dev'
# RUN /bin/bash -c 'pip3 install --upgrade pip'

# basic lib
RUN /bin/bash -c 'pip3 install numpy'
RUN /bin/bash -c 'pip3 install pandas'
RUN /bin/bash -c 'pip3 install matplotlib'

# ta-lib
RUN /bin/bash -c 'cd dependencies/ta-lib && ./configure'
RUN /bin/bash -c 'make -C $TALIB_DIR'
RUN /bin/bash -c 'make install -C $TALIB_DIR'
RUN /bin/bash -c 'pip3 install ta-lib'

RUN /bin/bash -c 'echo export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib >> ~/.bashrc'
RUN /bin/bash -c "sed -i -e 's/mesg\ n/tty\ -s\ \&\&\ mesg n/' ~/.profile"

# matplotlib
RUN /bin/bash -c 'mkdir -p ~/.config/matplotlib'
RUN /bin/bash -c 'touch ~/.config/matplotlib/matplotlibrc'
RUN /bin/bash -c 'echo export backend:Agg > ~/.config/matplotlib/matplotlibrc'

# tensorflow-gpu
RUN /bin/bash -c 'pip3 install tensorflow-gpu==1.4'